Package.describe({
  summary: 'Your package summary here.'
});

Package.on_use(function(api) {

  // add files for client
  api.add_files(
    [
      'source/Class.js',
      'source/Interface.js'
    ],
    'client'
  );

});