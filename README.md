# Headline for my Awesome Package

Short summary of my awesome package.

## Install with Meteorite
Install the module with: `mrt add my-awesome-package`

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [grunt](https://github.com/gruntjs/grunt).

## Release History
_(Nothing yet)_

## License
Copyright (c) 2013 Code Adventure
Licensed under the MIT license.
